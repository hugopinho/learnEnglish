package pt.sapo.dynip.hugopinho.aprenderingles.Fragments;

import android.media.MediaPlayer;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.Switch;

import pt.sapo.dynip.hugopinho.aprenderingles.R;

public class AnimaisFragment extends Fragment implements View.OnClickListener {

    private ImageButton btnCao, btnGato, btnLeao, btnMacaco, btnOvelha, btnVaca;
    private MediaPlayer mediaPlayer;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_animais, container, false);

        btnCao = view.findViewById(R.id.btnCao);
        btnGato = view.findViewById(R.id.btnGato);
        btnLeao = view.findViewById(R.id.btnLeao);
        btnMacaco = view.findViewById(R.id.btnMacaco);
        btnOvelha = view.findViewById(R.id.btnOvelha);
        btnVaca = view.findViewById(R.id.btnVaca);


        btnCao.setOnClickListener(this);
        btnGato.setOnClickListener(this);
        btnLeao.setOnClickListener(this);
        btnMacaco.setOnClickListener(this);
        btnOvelha.setOnClickListener(this);
        btnVaca.setOnClickListener(this);


        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnCao:
                mediaPlayer = MediaPlayer.create(getActivity(), R.raw.dog);
                reproduzirSom();
                break;
            case R.id.btnGato:
                mediaPlayer = MediaPlayer.create(getActivity(), R.raw.cat);
                reproduzirSom();
                break;
            case R.id.btnLeao:
                mediaPlayer = MediaPlayer.create(getActivity(), R.raw.lion);
                reproduzirSom();
                break;
            case R.id.btnMacaco:
                mediaPlayer = MediaPlayer.create(getActivity(), R.raw.monkey);
                reproduzirSom();
                break;
            case R.id.btnOvelha:
                mediaPlayer = MediaPlayer.create(getActivity(), R.raw.sheep);
                reproduzirSom();
                break;
            case R.id.btnVaca:
                mediaPlayer = MediaPlayer.create(getActivity(), R.raw.cow);
                reproduzirSom();
                break;
        }
    }

    public void reproduzirSom(){
        if(mediaPlayer !=null){
            mediaPlayer.start();
            //sempre que som é completo, libertamos o recurso
            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    mediaPlayer.release();
                }
            });
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(mediaPlayer !=null){
            mediaPlayer.release();
            mediaPlayer=null;
        }
    }
}